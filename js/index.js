// File: index.js

var search_base_url =
  "https://api.spotify.com/v1/search?query={artist_name}&type=artist&market=US&offset=0&limit=20";

// The following are just stored artist IDs for verification -- nothing to do here
//var artist_id = "0TnOYISbd1XYRBk9myaseg";  // Enrique Iglesias
//var artist_id = "3t58jfUhoMLYVO14XaUFLA";  // Pat Metheny
//var artist_id = "5olDKSsFhhmwh8UCWwKtpq";  // Chick Corea
//var artist_id = "6v0d6b30Aw6lK4AtZuOElo";  // Cuong Vu
//var artist_id = "31TPClRtHm23RisEBtV3X7";  // Timberlake

// ==============================================================================
// TASK 1: Obtain your own OAuth Token and replace this one -- this "Token"
//         will be expired by exam time and will NOT work -- just a sample!
//         (Note: Keep refreshing your Token about every 30 mins)
// ==============================================================================
var default_OAuthToken =
  "BQAyUGjT61SSB62At16uymclKg3NeVnCAu_o3HNxwL7eqaSWBDcxWXTsyVLwseSykl_b306VS4QBsUGS9wo5UsVpt2cuOUVHutJpObGqzP_MokYPHIg-naGkRcarD5vFCzdeRShA60EBQCmtHkwsF31PsYzXOry9bIZfZN0";

// =======================================================================================
// TASK 2: Complete and test this function that inserts an artist name into search url.
//         Use a JavaScript REGEX search and replace the {artist_name} template in the
//         above search_base_url use this function to concantenate (build) your search url
// =======================================================================================
function getSearchUrl(name) {
  var url = search_base_url;
  url = url.replace('{artist_name}', name);
  return url;
}

function api_search(artist_name, auth_token) {
  let oAuthToken = auth_token;
  let myurl = getSearchUrl(artist_name);
  console.log("searchURL: " + myurl);

  // =========================================================================
  // TASK 3: After verifying that the correct search URL is present
  //         after the api_search() is called, use JQuery $(select).manipulate
  //         to store this url in the input field selected by "input#search-url"
  // =========================================================================
  $('input#search-url').val(myurl);

  // AJAX call to get the Spotify Search Result
  $.ajax({
    method: "GET",
    url: myurl,
    dataType: "json",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: "Bearer " + oAuthToken
    },
    success: function(data) {
      console.log("success");
      console.log(data);

      // =========================================================================
      // TASK 4: - Add code to convert the raw data object into a
      //           JSON string before storing to the element identified by
      //           "textarea#spotify-data".  (This will be a useful string to
      //           copy-paste into an editor like VS Code to examine JSON data)
      // =========================================================================
      // <your code goes here>
      let myJSON = JSON.stringify(data);
      $("textarea#spotify-data").text(myJSON);

      // This is an example of how to traverse the data object to obtain
      // data about this artist (e.g. number of followers)
      let followers = data.artists.items[0].followers.total;
      let base = data.artists.items[0];
      console.log("Followers:" + followers);
      $("span#followers").text(followers);


      // =========================================================================
      // TASK 5: Add code to update the HTML field for the artist ID
      // =========================================================================
      $('input#artist-id').val(base.id);

      // =========================================================================
      // TASK 6: Add code to update the HTML field for the artist Name
      // =========================================================================
      $('span#out-name').text(base.name);


      // =========================================================================
      // TASK 7: Add code to update the HTML field for the artist Genres
      // =========================================================================
      $('span#genre').text(base.genres);



      // =========================================================================
      // TASK 8: Add code to update the HTML field for the artist URL and Image
      //         Use the images given by index 2 to produce an image that fits OK
      //         on this page.
      // =========================================================================
      $('span#artist-url').text(base.external_urls.spotify);
      $("img[alt='Artist']").attr('src', base.images[2].url);
    },
    error: function(e) {
      let err = JSON.stringify(e);
      console.log(err);
    },
    cache: false
  });
}


// THE MAIN PROGRAM STARTS HERE AFTER DOCUMENT LOAD
// Shorthand for $( document ).ready()
$(function() {
  console.log("Starting...");

  // Handle the Send API Request Button Click
  $("#send-req").click(function() {
    console.log("Send Button Hit!");
    let artist_name = $("#artist-name").val();
    console.log("Artist ID: " + artist_name);

    // Get default or input OAuth Token
    let auth_token = default_OAuthToken;
    if ($("input#auth-token").val() !== "") {
      auth_token = $("input#auth-token").val();
    }

    // Display the OAuth Token for us to see
    console.log("OAuth Token: " + auth_token);
    $("input#auth-token").val(auth_token);
    api_search(artist_name, auth_token);
  });
});
